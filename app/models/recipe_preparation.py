from app import db
from sqlalchemy.dialects.postgresql import UUID


class RecipePreparation(db.Model):
    __tablename__ = 'recipe_preparation'

    id = db.Column(UUID, primary_key=True, server_default=db.text('gen_random_uuid()'))
    step = db.Column(db.Integer, nullable=False)
    text = db.Column(db.Text)

    def __init__(self):
        pass
