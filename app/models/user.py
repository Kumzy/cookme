from app import db, login
from sqlalchemy.dialects.postgresql import UUID
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin

@login.user_loader
def load_user(id):
    return User.query.get(id)

class User(UserMixin,db.Model):
    __tablename__='user'

    id = db.Column(UUID(as_uuid=True), primary_key=True, server_default=db.text('gen_random_uuid()'))
    email = db.Column(db.Text, nullable=False, unique=True)
    username = db.Column(db.Text)
    firstname = db.Column(db.Text)
    lastname = db.Column(db.Text)
    created_on = db.Column(db.DateTime(timezone=True),server_default=db.text('now()'))
    #deferred() here is optional. Using it prevents the password_hash column from being fetched by default when a User object is loaded back from the database.
    #https://github.com/sqlalchemy/sqlalchemy/wiki/DatabaseCrypt
    password_hash = db.Column(db.Text, nullable=False)

    #recipes = db.relationship('Recipe', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)