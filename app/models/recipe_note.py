from app import db
from sqlalchemy.dialects.postgresql import UUID


class RecipeNote(db.Model):
    __tablename__ = 'recipe_note'

    id = db.Column(UUID, primary_key=True, server_default=db.text('gen_random_uuid()'))
    note = db.Column(db.Text)

    def __init__(self):
        pass
