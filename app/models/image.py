from app import db
from sqlalchemy.dialects.postgresql import UUID

class Image(db.Model):
    __tablename__ = 'image'

    id = db.Column(UUID, primary_key=True, server_default=db.text('gen_random_uuid()'))
    image = db.Column(db.LargeBinary, nullable=False)

    def __init__(self):
        pass
