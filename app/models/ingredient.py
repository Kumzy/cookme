from app import db
from sqlalchemy.dialects.postgresql import UUID

class Ingredient(db.Model):
    __tablename__ = 'ingredient'

    id = db.Column(UUID, primary_key=True, server_default=db.text('gen_random_uuid()'))
    name = db.Column(db.Text, nullable=False)
    name_plural = db.Column(db.Text)
    #image_id = db.Column(UUID, db.ForeignKey('image.id'))
    #image = db.relationship('Image', back_populates='image')

    def __init__(self):
        pass