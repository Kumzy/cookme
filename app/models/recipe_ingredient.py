from app import db
from sqlalchemy.dialects.postgresql import UUID


class RecipeIngredient(db.Model):
    __tablename__ = 'link_recipe_ingredient'

    id = db.Column(UUID, primary_key=True, server_default=db.text('gen_random_uuid()'))
    name = db.Column(db.Text, nullable=False)
    ingredient = db.Column(db.DateTime, nullable=False)
    quantity = db.Column(db.Numeric)
    additional_text = db.Column(db.Text)

    def __init__(self):
        pass
