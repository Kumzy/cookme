from app import db
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import text


class Unit(db.Model):
    __tablename__='unit'

    id = db.Column(UUID, primary_key=True, server_default=db.text('gen_random_uuid()'))
    name = db.Column(db.Text, nullable=False, unique=True)

    def __init__(self):
        pass
