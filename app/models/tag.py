from app import db
from sqlalchemy.dialects.postgresql import UUID

class Tag(db.Model):
    __tablename__ = 'tag'

    id = db.Column(UUID, primary_key=True, server_default=db.text('gen_random_uuid()'))
    name = db.Column(db.Text, nullable=False)

    def __init__(self):
        pass
