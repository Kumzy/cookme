from app import db
from sqlalchemy.dialects.postgresql import UUID


class Recipe(db.Model):
    __tablename__ = 'recipe'

    id = db.Column(UUID, primary_key=True, server_default=db.text('gen_random_uuid()'))
    name = db.Column(db.Text, nullable=False)
    creation_date = db.Column(db.DateTime)
    preparation_time = db.Column(db.Numeric)
    cooking_time = db.Column(db.Numeric)
    url =  db.Column(db.Text)
    #author_id = db.Column(UUID,db.ForeignKey('user.id'))

    def __init__(self):
        pass
