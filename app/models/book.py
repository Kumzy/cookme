from app import db
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import text


class Book(db.Model):
    __tablename__ = 'book'

    id = db.Column(UUID, primary_key=True, server_default=text('gen_random_uuid()'))
    name = db.Column(db.Text, nullable=False)
    #author_id = db.Column(UUID,db.ForeignKey('user.id'))

    def __init__(self):
        pass
