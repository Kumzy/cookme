from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager


app = Flask(__name__)
app.config.from_object(Config)
login = LoginManager(app)
login.login_view = 'login'
db = SQLAlchemy(app)
migrate = Migrate(app, db)


from app.routes import general
from app.routes import login

#Models import
from app.models.book import Book
from app.models.image import Image
from app.models.ingredient import Ingredient
from app.models.recipe import Recipe
from app.models.tag import Tag
from app.models.unit import Unit
from app.models.user import User
